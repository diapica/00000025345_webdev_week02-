function angka(num){
    if(document.getElementById('reset').value=="true"){
        reset();
        document.getElementById('layar').innerHTML = "";
        document.getElementById('reset').value = "false";
    }

    if(num=="AC"){
        reset();
    }else if(num=="+" || num =="-" || num=="x" || num =="÷" ){
        if(document.getElementById('angka1').value==""){
            alert("Masukin angka pertama dulu broo !!");
        }else if(document.getElementById("angka2").value!=""){
            alert("Operator sudah terpilih, silahkan reset jika ingin mengganti operator dengan menekan AC :)")
        }else{
            document.getElementById('operator').value=num;
        }
    }else{
        if(document.getElementById('operator').value==""){
            if(document.getElementById('angka1').value=="0"){
                document.getElementById('angka1').value=num;
            }else{
                document.getElementById('angka1').value+=num;
            }
            
        }else{
            if(document.getElementById('angka2').value=="0"){
                document.getElementById('angka2').value=num;
            }else{
                document.getElementById('angka2').value+=num;
            }
        }
    }
    angka1 = document.getElementById('angka1').value;
    angka2 = document.getElementById('angka2').value;
    operator = document.getElementById('operator').value;
    if(num=="AC"){
        document.getElementById('layar').innerHTML = "0";
    }else{
        document.getElementById('layar').innerHTML = angka1+" "+operator+" "+angka2;    
    }
}

function result(){
    var angka1 = document.getElementById('angka1').value;
    var angka2 = document.getElementById('angka2').value
    if(document.getElementById('angka2').value!=""){
        operator = document.getElementById('operator').value;
        if(operator=="+"){
            hasil =  +angka1 + +angka2;
        }else if(operator=="-"){
            hasil = angka1 - angka2;
        }else if(operator=="x"){
            hasil = angka1 * angka2;
        }else{
            hasil = angka1 / angka2;
        }  
        reset();
        document.getElementById('reset').value = "true";
        document.getElementById('layar').innerHTML += " = "+hasil+" ";
    }else{
        alert("Ga bisa broo, Coba lagii :) !!");
    }
    
}

function reset(){
    document.getElementById('angka1').value=""
    document.getElementById('angka2').value=""
    document.getElementById('operator').value=""
}
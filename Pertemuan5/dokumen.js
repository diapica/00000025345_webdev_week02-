document.getElementById("nama").innerHTML = 'Nama : ';
document.getElementById("chat").innerHTML = 'Chat : ';

var x = document.createElement("TEXTAREA");
x.setAttribute('placeholder','Nama');
x.setAttribute('width','1306px');
x.setAttribute('class','input_nama');
x.setAttribute('id','isi_nama');
document.getElementById("text_nama").appendChild(x);

var y = document.createElement("TEXTAREA");
y.setAttribute('placeholder','Chat');
y.setAttribute('class','input_chat');
y.setAttribute('width','1306px');
y.setAttribute('height','192px');
y.setAttribute('id','isi_chat');
document.getElementById("text_chat").appendChild(y);

var z = document.createElement('INPUT');
z.setAttribute('type','button');
z.setAttribute('value','SEND');
z.setAttribute('id','send');
z.setAttribute('onclick','kirim()');
document.getElementById("button_send").appendChild(z);

var a = document.createElement('INPUT');
a.setAttribute('type','button');
a.setAttribute('value','CLEAR !');
a.setAttribute('onclick','bersihkan()');
a.setAttribute('id','clear');
document.getElementById("button_clear").appendChild(a);

function bersihkan(){
    document.getElementById("tempat_chat").innerHTML = "";
    document.getElementById('tempat_chat').classList.add('tempat_chat');
    document.getElementById('tempat_chat').classList.remove('tempat_chat_scroll');
}

function kirim(){
    nama = document.getElementById('isi_nama').value;
    chat = document.getElementById('isi_chat').value;
    if(nama != "" && chat!=""){
        document.getElementById('tempat_chat').classList.remove('tempat_chat');
        document.getElementById('tempat_chat').classList.add('tempat_chat_scroll');
        var baru = document.createElement('div');
        baru.setAttribute('class','box_chat');
        tempat_chat = document.getElementById("tempat_chat")
        tempat_chat.insertBefore(baru,tempat_chat.childNodes[0]);
        var isi_chat = "<p class='nama_chat'>"+ nama +"</p><p> " + chat +"</p>";
        document.getElementsByClassName('box_chat')[0].innerHTML = isi_chat;
    }else{
        alert('Please Complete the Chat Form');
    }
}

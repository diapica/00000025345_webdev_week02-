$(document).ready(function(){
    $(".tombol").hide()
    $(".modal").hide();

    $(document).on('click','.kotak_chat_kiri',function(){
        nama = $(this).find('.nama').html();
        chat = $(this).find('.chat_isi').html();
        $(".modal").show();
        $(".isi_chatnya").append("<p class='nama'>"+nama+"</p>");
        $(".isi_chatnya").append("<p>"+chat+"</p>");
        
    });

    $(document).on('click','.kotak_chat_kanan',function(){
        nama = $(this).find('.nama').html();
        chat = $(this).find('.chat_isi').html();
        $(".modal").show();
        $(".isi_chatnya").append("<p class='nama'>"+nama+"</p>");
        $(".isi_chatnya").append("<p>"+chat+"</p>");
    });

    $(".textbox").focus(function(){
        id = $(this).attr('id');
        if(id=='nama1'){
            $("#tombol1").show()
        }else if(id=="nama2"){
            $("#tombol2").show()
        }
    })

    $(".tombol").click(function(){
        $("#tempat_chat").removeClass("tempat_chat");
        $("#tempat_chat").addClass("tempat_chat_scroll");
        id = $(this).attr('id');
        $("#tombol3").show();
        if(id=="tombol1"){
            nama = $("#nama1").val();
            chat = $("#chat1").val();
            $(".tempat_chat_scroll").prepend("<div class='kotak_chat_kiri'></div>");
            $(".kotak_chat_kiri:first").append("<div class='nama'>"+nama+"</div>");
            $(".kotak_chat_kiri:first").append("<div class='chat_isi'>"+chat+"</div>");
        }else if(id=="tombol2"){
            nama = $("#nama2").val();
            chat = $("#chat2").val();
            $(".tempat_chat_scroll").prepend("<div class='kotak_chat_kanan'></div>");
            $(".kotak_chat_kanan:first").append("<div class='nama'>"+nama+"</div>");
            $(".kotak_chat_kanan:first").append("<div class='chat_isi'>"+chat+"</div>");
        }else{
            $("#tombol3").hide();
            $(".tempat_chat_scroll").html("");
            $("#tempat_chat").removeClass("tempat_chat_scroll");
            $("#tempat_chat").addClass("tempat_chat");
        }
    })

    $(".close").click(function(){
        $(".modal").hide();
        $(".isi_chatnya").html("");
    })
})